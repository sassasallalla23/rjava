
/**
 * Write a description of class CodeGenerator here.
 *
 * @author (your name)
 * @version (a version number or a date)
 */

import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Stack;


public class CodeGenerator
{
    /*private*/ public StringBuilder builder;
    private int labelCount;
    private String className;
    private int maxStackSize;
    private int stackSize;

    public CodeGenerator(String className) {
        builder = new StringBuilder();
        labelCount = 0;
        this.className = className;
        stackSize = 0;
        maxStackSize = 0;
    }

    public String generateLabel() {
        return String.format("label%d", labelCount++);
    }

    public void appendInstruction(String instruction, int stackDifference) {
        stackSize += stackDifference;
        maxStackSize = Math.max(stackSize, maxStackSize);
        builder.append(String.format("%s\n", instruction));
    }

    public static String pushIntInstruction(int value) {
        if (value == -1) {
            return "iconst_m1";
        } else if (0 <= value && value <= 5) {
            return String.format("iconst_%d", value);
        } else if (-Math.pow(2,7) <= value && value <= Math.pow(2,7) - 1) {
            return String.format("bipush %d", value);
        } else if (-Math.pow(2,15) <= value && value <= Math.pow(2,15)-1) {
            return String.format("sipush %d", value);
        } else {
            return String.format("ldc %d", value);
        }
    }

    public void pushComparisonInstruction(String instruction) {
        String elseLabel = generateLabel();
        String endLabel = generateLabel();
        appendInstruction(String.format("%s %s", instruction, elseLabel), -2);
        appendInstruction(pushIntInstruction(1), +1);
        appendInstruction(String.format("goto %s", endLabel), -1);
        appendInstruction(String.format("%s:", elseLabel), 0);
        appendInstruction(pushIntInstruction(0), +1);
        appendInstruction(String.format("%s:", endLabel), 0);
    }

    public void writeToFile(int maxVariableCount, Statement body) {
        builder.append(String.format(".class public %s\n", className));
        builder.append(".super java/lang/Object\n");
        builder.append(".method public <init>()V\n");
        builder.append("    aload_0\n");
        builder.append("    invokenonvirtual java/lang/Object/<init>()V\n");
        builder.append("    return\n");
        builder.append(".end method\n");
        
        builder.append(".method public static main([Ljava/lang/String;)V\n");
        builder.append(String.format(".limit locals %d\n", maxVariableCount));
        builder.append(".limit stack \n");
        int stackLimitIndex = builder.length() - 1;
        body.emit(this);
        builder.append("return\n");
        builder.append(".end method\n");
        builder.insert(stackLimitIndex, maxStackSize);
        
        try {
            Files.write(Paths.get(String.format("%s.j", className)), builder.toString().getBytes());
        } catch(Exception e) {
            Errors.report(String.format("ERROR: Could not write to file \"%s.j\": %s", className, e.toString()));
        }
    }
}
