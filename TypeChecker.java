

/**
 * Write a description of class TypeChecker here.
 *
 * @author Johannes Bockhorst
 * @version 0.1
 */

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Stack;
import java.util.Arrays;

enum PrimitiveType {
    Int,
    Boolean,
    Void,
    Char
}

class Type {
    public PrimitiveType primitiveType;
    
    public Type(PrimitiveType primitiveType) {
        this.primitiveType = primitiveType;
    }
    
    public boolean equals(Type other) {
        return primitiveType == other.primitiveType;
    }
    
    public String toString() {
        return primitiveType.toString();
    }
}

class Types {
    public static final Type INT = new Type(PrimitiveType.Int);
    public static final Type BOOLEAN = new Type(PrimitiveType.Boolean);
    public static final Type VOID = new Type(PrimitiveType.Void);
    public static final Type CHAR = new Type(PrimitiveType.Char);
    public static Type fromToken(Token t) {
        switch (t.type) {
            case IntKeyword: return INT;
            case BooleanKeyword: return BOOLEAN;
            case CharKeyword: return CHAR;
            default: return null;
        }
    }
}

abstract class Symbol {
    public Symbol() {}
}

class VariableSymbol extends Symbol {
    public Type type;
    public int id;
    
    public VariableSymbol(Type type, int id) {
        super();
        this.type = type;
        this.id = id;
    }
}

class Function {
    public Type returnType;
    public ArrayList<Type> parameterTypes;
    
    public Function(Type returnType, ArrayList<Type> parameterTypes) {
        super();
        this.returnType = returnType;
        this.parameterTypes = parameterTypes;
    }
}

class FunctionSymbol extends Symbol {
    ArrayList<Function> overloads;
    public FunctionSymbol(ArrayList<Function> overloads) {
        this.overloads = overloads;
    }
}

class BinaryOperator {
    public TokenType operator;
    public Type leftType;
    public Type rightType;
    public Type resultType;
    
    public BinaryOperator(TokenType operator, Type leftType, Type rightType, Type resultType) {
        this.operator = operator;
        this.leftType = leftType;
        this.rightType = rightType;
        this.resultType = resultType;
    }
}

class UnaryOperator {
    public TokenType operator;
    public Type operandType;
    public Type resultType;
    
    public UnaryOperator(TokenType operator, Type operandType, Type resultType) {
        this.operator = operator;
        this.operandType = operandType;
        this.resultType = resultType;
    }
}

public class TypeChecker
{
    public static BinaryOperator[] binaryOperators = new BinaryOperator[] {
        new BinaryOperator(TokenType.Plus, Types.INT, Types.INT, Types.INT),
        new BinaryOperator(TokenType.Minus, Types.INT, Types.INT, Types.INT),
        new BinaryOperator(TokenType.Star, Types.INT, Types.INT, Types.INT),
        new BinaryOperator(TokenType.Slash, Types.INT, Types.INT, Types.INT),
        new BinaryOperator(TokenType.Percent, Types.INT, Types.INT, Types.INT),
        
        new BinaryOperator(TokenType.And, Types.INT, Types.INT, Types.INT),
        new BinaryOperator(TokenType.Pipe, Types.INT, Types.INT, Types.INT),
        new BinaryOperator(TokenType.GreaterGreater, Types.INT, Types.INT, Types.INT),
        new BinaryOperator(TokenType.LessLess, Types.INT, Types.INT, Types.INT),
        new BinaryOperator(TokenType.Caret, Types.INT, Types.INT, Types.INT),
        
        new BinaryOperator(TokenType.Less, Types.INT, Types.INT, Types.BOOLEAN),
        new BinaryOperator(TokenType.LessEqual, Types.INT, Types.INT, Types.BOOLEAN),
        new BinaryOperator(TokenType.Greater, Types.INT, Types.INT, Types.BOOLEAN),
        new BinaryOperator(TokenType.GreaterEqual, Types.INT, Types.INT, Types.BOOLEAN),
        new BinaryOperator(TokenType.EqualEqual, Types.INT, Types.INT, Types.BOOLEAN),
        new BinaryOperator(TokenType.BangEqual, Types.INT, Types.INT, Types.BOOLEAN),
        
        new BinaryOperator(TokenType.AndAnd, Types.BOOLEAN, Types.BOOLEAN, Types.BOOLEAN),
        new BinaryOperator(TokenType.PipePipe, Types.BOOLEAN, Types.BOOLEAN, Types.BOOLEAN),    
        
        new BinaryOperator(TokenType.EqualEqual, Types.BOOLEAN, Types.BOOLEAN, Types.BOOLEAN),
        new BinaryOperator(TokenType.BangEqual, Types.BOOLEAN, Types.BOOLEAN, Types.BOOLEAN),
    };
    
    public static UnaryOperator[] unaryOperators = new UnaryOperator[] {
        new UnaryOperator(TokenType.Minus, Types.INT, Types.INT),
        new UnaryOperator(TokenType.Plus, Types.INT, Types.INT),
        new UnaryOperator(TokenType.Tilde, Types.INT, Types.INT),
        
        new UnaryOperator(TokenType.Bang, Types.BOOLEAN, Types.BOOLEAN),
    };
    
    public static Type getBinaryOperatorType(TokenType operator, Type leftType, Type rightType) {
        for (BinaryOperator binaryOperator : binaryOperators) {
            if (binaryOperator.operator == operator &&
                binaryOperator.leftType.equals(leftType) &&
                binaryOperator.rightType.equals(rightType)) {
                return binaryOperator.resultType;
            }
        }
        return null;
    }
    
    public static Type getUnaryOperatorType(TokenType operator, Type operandType) {
        for (UnaryOperator unaryOperator : unaryOperators) {
            if (unaryOperator.operator == operator && unaryOperator.operandType.equals(operandType)) {
                return unaryOperator.resultType;
            }
        }
        return null;
    }
    
    private ArrayList<Hashtable<String, Symbol>> scope;
    private int variableCount;
    public int maxVariableCount;
    public Stack<WhileStatement> whileStatementStack;
    
    public TypeChecker() {
        variableCount = 1;
        maxVariableCount = 1;
        scope = new ArrayList<>();
        scope.add(new Hashtable<>());

        scope.get(0).put("println", new FunctionSymbol(
                    new ArrayList<>(Arrays.asList(
                            new Function(Types.VOID, new ArrayList<>(Arrays.asList(Types.INT))),
                            new Function(Types.VOID, new ArrayList<>(Arrays.asList(Types.CHAR)))))));
        scope.get(0).put("print", new FunctionSymbol(
                    new ArrayList<>(Arrays.asList(
                            new Function(Types.VOID, new ArrayList<>(Arrays.asList(Types.INT))),
                            new Function(Types.VOID, new ArrayList<>(Arrays.asList(Types.CHAR)))))));


        whileStatementStack = new Stack<>();
    }
    
    public void setVariableCount(int count) {
        variableCount = count;
        maxVariableCount = Math.max(maxVariableCount, variableCount);
    }
    
    public int getVariableCount() {
        return variableCount;
    }
    
    public Symbol getSymbol(String name) {
        for (int i = scope.size() - 1; i >= 0; i--) {
            Symbol result = scope.get(i).get(name);
            if (result != null) {
                return result;
            }
        }
        return null;
    }
    
    public void addVariable(String name, Type type) {
        //System.out.printf("Variable %s: %d\n", name, getVariableCount());
        scope.get(scope.size() - 1).put(name, new VariableSymbol(type, getVariableCount()));
        setVariableCount(getVariableCount()+1);
    }
    
    public void pushScope() {
        scope.add(new Hashtable<>());
    }
    
    public void popScope() {
        scope.remove(scope.size() - 1);
    }
}
