
/**
 * Write a description of class Main here.
 *
 * @author Johannes Bockhorst
 * @version 0.1
 */

/*
TODO:
- Comments
- more operators: bitwise (&, |, ~, <<, >>)
- arrays
    - indexed access expressions (x[y])
    - member access  expressions (x.length)
- classes
    - methods -> more complex call expressions (x.y())
*/

import java.util.ArrayList;

public class Main {
    public static void main(String[] args) {
        String inputFileName = "prototype.rjava";
        String className = "HelloWorld";
        if (args.length >= 2) {
            inputFileName = args[0];
            className = args[1];
        }
        Lexer lexer = new Lexer(inputFileName);
        //Lexer lexer = new Lexer("tests/helloWorld.rjava");
        ArrayList<Token> tokens = lexer.collectTokens();
        //for (int i = 0; i < tokens.size(); i++) {
        //    System.out.println(tokens.get(i).toString());
        //}
        Parser parser = new Parser(tokens);
        
        // Expression e = parser.parseExpression();
        // e.resolveType(new TypeChecker());
        // e.print();
        // CodeGenerator cg = new CodeGenerator("Main");
        // e.emit(cg);
        // System.out.print(cg.builder);
        
        Statement s = parser.parseStatement();
        //s.print();
        TypeChecker t = new TypeChecker();
        s.typeCheck(t);
        CodeGenerator cg = new CodeGenerator(className);
        cg.writeToFile(t.maxVariableCount, s);
    }
}
