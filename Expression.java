
/**
 * Write a description of class Expression here.
 *
 * @author (your name)
 * @version (a version number or a date)
 */
import java.util.ArrayList;

class Printing {
    public static void printIndentation(int indentation) {
        for (int i = 0; i < indentation; i++) System.out.print("\t");
    }
}

public abstract class Expression {
    public Location location;
    public Type type;

    public Expression(Location location) {
        this.location = location;
    }

    public void print() {
        print(0);
    }

    abstract public void print(int indentation);

    abstract public Type resolveType(TypeChecker typeChecker);

    abstract public void emit(CodeGenerator codeGenerator);
}

class BinaryExpression extends Expression {
    public Expression left;
    public Token operator;
    public Expression right;

    public BinaryExpression(Expression left, Token operator, Expression right) {
        super(left.location);
        this.left = left;
        this.right = right;
        this.operator = operator;
    }

    @Override
    public void print(int indentation) {
        Printing.printIndentation(indentation);
        System.out.printf("BinaryExpression(%s) %s\n", operator.text, type == null ? "" : type.toString());
        left.print(indentation + 1);
        right.print(indentation + 1);
    }

    @Override
    public Type resolveType(TypeChecker typeChecker) {
        Type leftType = left.resolveType(typeChecker);
        Type rightType = right.resolveType(typeChecker);
        Type result = typeChecker.getBinaryOperatorType(operator.type, leftType, rightType);
        if (result == null) {
            Errors.reportAtLocation(String.format("TYPE_ERROR: Binary operator <%s> is not defined for types <%s> and <%s>", operator.type, leftType.toString(), rightType.toString()), location);
        }
        type = result;
        return result;
    }

    @Override
    public void emit(CodeGenerator codeGenerator) {
        if (operator.type != TokenType.AndAnd && operator.type != TokenType.PipePipe) {
            left.emit(codeGenerator);
            right.emit(codeGenerator);
        }
        switch(operator.type) {
            case Star: codeGenerator.appendInstruction("imul", -1); break;
            case Slash: codeGenerator.appendInstruction("idiv", -1); break; // TODO: add divide-by-zero checks
            case Percent: codeGenerator.appendInstruction("irem", -1); break;
            case Plus: codeGenerator.appendInstruction("iadd", -1); break;
            case Minus: codeGenerator.appendInstruction("isub", -1); break;
            case And: codeGenerator.appendInstruction("iand", -1); break;
            case Pipe: codeGenerator.appendInstruction("ior", -1); break;
            case Caret: codeGenerator.appendInstruction("ixor", -1); break;

            // TODO: optimization
            case Less: {
                codeGenerator.pushComparisonInstruction("if_icmpge");
            } break;
            case Greater: {
                codeGenerator.pushComparisonInstruction("if_icmple");
            } break;
            case LessEqual: {
                codeGenerator.pushComparisonInstruction("if_icmpgt");
            } break;
            case GreaterEqual: {
                codeGenerator.pushComparisonInstruction("if_icmplt");
            } break;
            case EqualEqual: {
                codeGenerator.pushComparisonInstruction("if_icmpne");
            } break;
            case BangEqual: {
                codeGenerator.pushComparisonInstruction("if_icmpeq");
            } break;
            case AndAnd: {
                left.emit(codeGenerator);
                String falseLabel = codeGenerator.generateLabel();
                String endLabel = codeGenerator.generateLabel();
                codeGenerator.appendInstruction(String.format("ifeq %s", falseLabel), -1);
                right.emit(codeGenerator);
                codeGenerator.appendInstruction(String.format("ifeq %s", falseLabel), -1);
                codeGenerator.appendInstruction(codeGenerator.pushIntInstruction(1), +1);
                codeGenerator.appendInstruction(String.format("goto %s", endLabel), -1);
                codeGenerator.appendInstruction(String.format("%s:", falseLabel), 0);
                codeGenerator.appendInstruction(codeGenerator.pushIntInstruction(0), +1);
                codeGenerator.appendInstruction(String.format("%s:", endLabel), 0);
            } break;
            case PipePipe: {
                left.emit(codeGenerator);
                String trueLabel = codeGenerator.generateLabel();
                String endLabel = codeGenerator.generateLabel();
                codeGenerator.appendInstruction(String.format("ifne %s", trueLabel), -1);
                right.emit(codeGenerator);
                codeGenerator.appendInstruction(String.format("ifne %s", trueLabel), -1);
                codeGenerator.appendInstruction(codeGenerator.pushIntInstruction(0), +1);
                codeGenerator.appendInstruction(String.format("goto %s", endLabel), -1);
                codeGenerator.appendInstruction(String.format("%s:", trueLabel), 0);
                codeGenerator.appendInstruction(codeGenerator.pushIntInstruction(1), +1);
                codeGenerator.appendInstruction(String.format("%s:", endLabel), 0);
            } break;
            default:
            Errors.report("TODO");
        }
    }
}

class UnaryExpression extends Expression {
    public Token operator;
    public Expression operand;

    public UnaryExpression(Token operator, Expression operand) {
        super(operator.location);
        this.operator = operator;
        this.operand = operand;
    }

    @Override
    public void print(int indentation) {
        Printing.printIndentation(indentation);
        System.out.printf("UnaryExpression(%s) %s\n", operator.text, type == null ? "" : type.toString());
        operand.print(indentation + 1);
    }

    @Override
    public Type resolveType(TypeChecker typeChecker) {
        Type operandType = operand.resolveType(typeChecker);
        Type result = typeChecker.getUnaryOperatorType(operator.type, operandType);
        if (result == null) {
            Errors.reportAtLocation(String.format("TYPE_ERROR: Unary operator <%s> is not defined for opearand of type <%s>", operator.type, operandType.toString()), location);
        }
        type = result;
        return result;
    }

    @Override
    public void emit(CodeGenerator codeGenerator) {
        operand.emit(codeGenerator);
        switch(operator.type) {
            case Plus: return;
            case Minus:
            codeGenerator.appendInstruction("ineg", 0);
            break;
            case Bang:
            codeGenerator.appendInstruction("ineg", 0);
            codeGenerator.appendInstruction("iconst_1", +1);
            codeGenerator.appendInstruction("iadd", -1);
            case Tilde:
            codeGenerator.pushIntInstruction(-1);
            codeGenerator.appendInstruction("ixor", -1);
            break;
            default:
            Errors.report("Unimplemented operator in UnaryExpression.emit");
            break;
        }
    }
}

class LiteralExpression extends Expression {
    public Token literal;

    public LiteralExpression(Token literal) {
        super(literal.location);
        this.literal = literal;
    }

    @Override
    public void print(int indentation) {
        Printing.printIndentation(indentation);
        System.out.printf("LiteralExpression(%s) %s\n", literal.text, type == null ? "" : type.toString());
    }

    @Override
    public Type resolveType(TypeChecker typeChecker) {
        Type result;
        switch (literal.type) {
            case IntLiteral: result = Types.INT; break;
            case TrueKeyword:
            case FalseKeyword: result = Types.BOOLEAN; break;
            case CharLiteral: result = Types.CHAR; break;
            default:
            Errors.reportAtLocation(String.format("TYPE_ERROR: Unknown literal with token of type <%s>", literal.type), location);
            return null;
        }
        type = result;
        return result;
    }

    @Override
    public void emit(CodeGenerator codeGenerator) {
        int value = 0;
        switch(literal.type) {
            case IntLiteral:
            try {
                value = Integer.parseInt(literal.text);
            } catch (Exception e) {
                Errors.reportAtLocation("PARSE_ERROR: Integer literal is not a valid integer", location);
            }
            break;
            case TrueKeyword: value = 1; break;
            case FalseKeyword: value = 0; break;
            case CharLiteral: value = (int)(literal.text.charAt(1)); break;
            default:
            Errors.report("Unimplemented literal type in LiteralExpression.emit");
            break;
        }
        codeGenerator.appendInstruction(codeGenerator.pushIntInstruction(value), +1);
    }
}

class VariableExpression extends Expression {
    public Token variable;
    public int id;

    public VariableExpression(Token variable) {
        super(variable.location);
        this.variable = variable;
        id = 0;
    }

    @Override
    public void print(int indentation) {
        Printing.printIndentation(indentation);
        System.out.printf("VariableExpression(%s) %s\n", variable.text, type == null ? "" : type.toString());
    }

    @Override
    public Type resolveType(TypeChecker typeChecker) {
        Symbol s = typeChecker.getSymbol(variable.text);
        if (s == null) {
            Errors.reportAtLocation(String.format("TYPE_ERROR: Variable '%s' not found in current scope", variable.text), location);
            return null;
        }
        if (s instanceof VariableSymbol) {
            VariableSymbol variableSymbol = (VariableSymbol)s;
            type = variableSymbol.type;
            id = variableSymbol.id;
            //System.out.printf("Expression %s: %d\n", variable.text, id);
            return type;
        } else {
            Errors.reportAtLocation(String.format("TYPE_ERROR: '%s' is not a variable", s), location);
            return null;
        }
    }

    @Override
    public void emit(CodeGenerator codeGenerator) {
        codeGenerator.appendInstruction(String.format("iload%c%d", id <= 3 ? '_' : ' ', id), +1);
    }
}

class CallExpression extends Expression {
    public Token name;
    public ArrayList<Expression> parameters;

    public CallExpression(Token name, ArrayList<Expression> parameters) {
        super(name.location);
        this.name = name;
        this.parameters = parameters;
    }

    @Override
    public void print(int indentation) {
        Printing.printIndentation(indentation);
        System.out.printf("CallExpression(%s) %s\n", name.text, type == null ? "" : type.toString());
        for (Expression parameter : parameters) {
            parameter.print(indentation + 1);
        }
    }

    @Override
    public Type resolveType(TypeChecker typeChecker) {
        Symbol s = typeChecker.getSymbol(name.text);
        if (s == null) {
            Errors.reportAtLocation(String.format("TYPE_ERROR: No function '%s' declared in the current scope", name.text), location);
        } else if (!(s instanceof FunctionSymbol)) {
            Errors.reportAtLocation(String.format("TYPE_ERROR: '%s' is not a function", name.text), location);
        }

        FunctionSymbol functionSymbol = (FunctionSymbol)s;
        boolean foundOverload = false;
        for (Function function : functionSymbol.overloads) {
            if (parameters.size() != function.parameterTypes.size()) {
                continue;// Errors.reportAtLocation(String.format("TYPE_ERROR: Invalid number of arguments for call of function '%s'", name.text), location);
            }

            boolean hasSameParameters = true;
            for (int i = 0; i < parameters.size(); i++) {
                Type expected = function.parameterTypes.get(i);
                Expression parameter = parameters.get(i);
                Type found = parameter.resolveType(typeChecker);
                if (!found.equals(expected)) {
                    //Errors.reportAtLocation(String.format("TYPE_ERROR: Parameter #%d in call of function '%s' does not have the expected type. Expected <%s> but got <%s>", name.text, found.toString(), expected.toString()), parameter.location);
                    hasSameParameters = false;
                    break;
                }
            }
            if (hasSameParameters) {
                type = function.returnType;
                return type;
            }
        }
        Errors.reportAtLocation(String.format("TYPE_ERROR: No matching overload was found for function '%s'", name.text), location);
        return type;
    }

    @Override
    public void emit(CodeGenerator codeGenerator) {
        if (name.text.equals("println")) {
            codeGenerator.appendInstruction("getstatic java/lang/System.out Ljava/io/PrintStream;", +1);
            parameters.get(0).emit(codeGenerator);
            
            codeGenerator.appendInstruction(
                    String.format("invokevirtual java/io/PrintStream.println(%c)V", parameters.get(0).type.equals(Types.CHAR) ? 'C' : 'I'),
                    -2);
        } else if (name.text.equals("print")) {
            codeGenerator.appendInstruction("getstatic java/lang/System.out Ljava/io/PrintStream;", +1);
            parameters.get(0).emit(codeGenerator);
            
            codeGenerator.appendInstruction(
                    String.format("invokevirtual java/io/PrintStream.print(%c)V", parameters.get(0).type.equals(Types.CHAR) ? 'C' : 'I'),
                    -2);
        } else {
            Errors.report(String.format("Unimplemented function %s", name.text));
        }
    }
}

class AssignmentExpression extends Expression {
    public Expression lValue;
    public Token operator;
    public Expression rValue;

    public AssignmentExpression(Expression lValue, Token operator, Expression rValue) {
        super(lValue.location);
        this.operator = operator;
        this.lValue = lValue;
        this.rValue = rValue;
    }

    @Override
    public void print(int indentation) {
        Printing.printIndentation(indentation);
        System.out.printf("Assignement(%s)\n", operator.text);
        lValue.print(indentation + 1);
        rValue.print(indentation + 1);
    }

    @Override
    public Type resolveType(TypeChecker typeChecker) {
        if (!(lValue instanceof VariableExpression)) {
            Errors.reportAtLocation("TYPE_ERROR: Unexpected expression on left side of assignment expression", location);
        }
        Type leftType = lValue.resolveType(typeChecker);
        Type rightType = rValue.resolveType(typeChecker);
        if (!leftType.equals(rightType)) {
            Errors.reportAtLocation(String.format("TYPE_ERROR: Left side (<%s>) of assignment does not match ride side (<%s>) in type", leftType.toString(), rightType.toString()), location);
        }
        type = leftType;
        return type;
    }

    @Override
    public void emit(CodeGenerator codeGenerator) {
        if (lValue instanceof VariableExpression) {
            
            if (lValue.type.equals(Types.INT) || lValue.type.equals(Types.BOOLEAN) || lValue.type.equals(Types.CHAR)) {
                rValue.emit(codeGenerator);
                codeGenerator.appendInstruction("dup", +1);
                int id = ((VariableExpression)lValue).id;
                codeGenerator.appendInstruction(String.format("istore%c%d", id <= 3 ? '_' : ' ', id), -1);
            } else {
                Errors.report(String.format("Unimplemented type for variable %s", lValue.type.toString()));
            }
        } else {
            Errors.report(String.format("Unimplemented lValue of type %s", lValue));
        }
    }
}

class CastExpression extends Expression {
    public Token typeName;
    public Expression operand;

    public CastExpression(Token typeName, Expression operand) {
        super(typeName.location);
        this.typeName = typeName;
        this.operand = operand;
    }

    @Override
    public void print(int indentation) {
        Printing.printIndentation(indentation);
        System.out.printf("Cast(%s)", typeName.text);
        operand.print(indentation + 1);
    }

    @Override
    public Type resolveType(TypeChecker typeChecker) {
        Type operandType = operand.resolveType(typeChecker);
        Type castType = Types.fromToken(typeName);
        if (castType == null) {
            Errors.reportAtLocation(String.format("TYPE_ERROR: Unknown type <%s>", typeName.text), typeName.location);
        }
        if (castType.equals(operandType)) {
            Errors.reportAtLocation("TYPE_ERROR: Unnecessary cast", location);
        }
        // This should be abstracted but there are only two possibilities so it's unnecessary
        if ((operandType.equals(Types.CHAR) && castType.equals(Types.INT)) ||
                (operandType.equals(Types.INT) && castType.equals(Types.CHAR))) {
            type = castType;
            return type;
        } else {
            Errors.reportAtLocation(String.format("TYPE_ERROR: Invalid conversion from type <%s> to <%s>", operandType.toString(), castType.toString()), location);
            return null;
        }
    }

    @Override
    public void emit(CodeGenerator codeGenerator) {
        operand.emit(codeGenerator);
        if (type.equals(Types.CHAR) && operand.type.equals(Types.INT)) {
            codeGenerator.appendInstruction("i2c", 0);
        } else if (type.equals(Types.INT) && operand.type.equals(Types.CHAR)){
            ////codeGenerator.appendInstruction("c2i", -1);
        }
    }
}
