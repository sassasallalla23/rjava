
/**
 * Write a description of class Parser here.
 *
 * @author Johannes Bockhorst
 * @version 0.1
 */

import java.util.ArrayList;

public class Parser {
    ArrayList<Token> tokens;
    int position;

    public static int getBinaryPrecedence(TokenType type) {
        switch (type) {
            case Star:
            case Slash:
            case Percent: return 7;
            case Plus:
            case Minus: return 6;
            case LessLess:
            case GreaterGreater: return 5;
            case Less:
            case LessEqual:
            case Greater:
            case GreaterEqual: return 4;
            case EqualEqual:
            case BangEqual: return 3;
            case And:
            case Caret:
            case Pipe: return 2;
            case AndAnd:
            case PipePipe: return 1;
            default: return 0;
        }
    }

    public static boolean isUnaryOperator(TokenType type) {
        switch (type) {
            case Plus:
            case Minus:
            case Tilde:
            case Bang: return true;
            default: return false;
        }
    }

    public static boolean isAssignmentOperator(TokenType type) {
        switch (type) {
            case Equal: return true;
            default: return false;
        }
    }

    public static boolean isTypeKeyword(TokenType type) {
        switch(type) {
            case IntKeyword:
            case BooleanKeyword:
            case CharKeyword:
            return true;
            default: return false;
        }
    }

    public Parser(ArrayList<Token> tokens) {
        this.tokens = tokens;
        position = 0;
    }

    public Token peekToken() {
        return peekToken(0);
    }

    public Token peekToken(int offset) {
        int index = position + offset;
        if (index >= tokens.size() || index < 0)
            return tokens.get(tokens.size()-1);
        else
            return tokens.get(index);
    }

    public Token consumeToken() {
        Token t = peekToken();
        position += 1;
        return t;
    }

    public Token expectToken(TokenType expectedType, String message) {
        Token t = consumeToken();
        if (t.type != expectedType) {
            Errors.reportAtLocation(
                String.format("PARSE_ERROR: Expected token of type <%s> but got <%s> %s",
                    t.type.toString(), expectedType.toString(), message),
                t.location);
        }
        return t;
    }

    public Token expectToken(TokenType expectedType) {
        return expectToken(expectedType, "");
    }

    public Expression parseExpression() {
        return parseBinaryExpression(0);
    }

    public Expression parseBinaryExpression(int parentPrecedence) {
        Expression left = parsePrimaryExpression();
        Token operator = peekToken();

        if (isAssignmentOperator(operator.type) && parentPrecedence == 0) {
            consumeToken();
            Expression rValue = parseExpression();
            return new AssignmentExpression(left, operator, rValue);
        }

        int precedence = getBinaryPrecedence(operator.type);
        while (precedence > 0 && precedence >= parentPrecedence) {
            consumeToken();
            Expression right = parseBinaryExpression(precedence);
            left = new BinaryExpression(left, operator, right);
            operator = peekToken();
            precedence = getBinaryPrecedence(operator.type);
        }

        return left;
    }

    public Expression parsePrimaryExpression() {
        Token next = peekToken();
        switch (next.type) {
            case IntLiteral:
            case CharLiteral:
            return new LiteralExpression(consumeToken());
            case TrueKeyword:
            case FalseKeyword:
            return new LiteralExpression(consumeToken());
            case Variable: {
                Token variable = consumeToken();
                if (peekToken().type == TokenType.OpenParenthesis) {
                    ArrayList<Expression> parameters = new ArrayList<>();
                    do {
                        consumeToken();
                        Expression p = parseExpression();
                        parameters.add(p);
                    } while (peekToken().type == TokenType.Comma);
                    expectToken(TokenType.CloseParenthesis, "at the end of call expression");
                    return new CallExpression(variable, parameters);
                } else {
                    return new VariableExpression(variable);
                }
            }
            case OpenParenthesis: {
                consumeToken();
                if (isTypeKeyword(peekToken().type)) {
                    Token typeName = consumeToken();
                    expectToken(TokenType.CloseParenthesis, "after type name at the start of cast expression");
                    Expression casted = parsePrimaryExpression();
                    return new CastExpression(typeName, casted);
                } else {
                    Expression parenthesized = parseExpression();
                    expectToken(TokenType.CloseParenthesis, "at the end of parenthesized expression");
                    return parenthesized;
                }
            }
            default:
            if (isUnaryOperator(next.type)) {
                Token operator = consumeToken();
                Expression operand = parsePrimaryExpression();
                return new UnaryExpression(operator, operand);
            }
            Errors.reportAtLocation(String.format("PARSE_ERROR: Unexpected token of type <%s> at start of primary expression", next.type), next.location);
            break;
        }
        return null;
    }

    public Statement parseStatement() {
        Token next = peekToken();
        switch (next.type) {
            case IfKeyword: {
                consumeToken();
                expectToken(TokenType.OpenParenthesis);
                Expression condition = parseExpression();
                expectToken(TokenType.CloseParenthesis);
                Statement body = parseStatement();
                next = peekToken();
                Statement elseBody = null;
                if (next.type == TokenType.ElseKeyword) {
                    consumeToken();
                    elseBody = parseStatement();
                }
                return new IfStatement(condition, body, elseBody);
            }
            case BreakKeyword: {
                Location location = consumeToken().location;
                expectToken(TokenType.SemiColon);
                return new BreakStatement(location);
            }
            case ContinueKeyword: {
                Location location = consumeToken().location;
                expectToken(TokenType.SemiColon);
                return new ContinueStatement(location);
            }
            case WhileKeyword: {
                consumeToken();
                expectToken(TokenType.OpenParenthesis);
                Expression condition = parseExpression();
                expectToken(TokenType.CloseParenthesis);
                Statement body = parseStatement();
                return new WhileStatement(condition, body);
            }
            case OpenCurly: {
                Token open = consumeToken();
                ArrayList<Statement> statements = new ArrayList<>();
                while (peekToken().type != TokenType.CloseCurly) {
                    int startPos = position;
                    statements.add(parseStatement());
                    if (startPos == position) {
                        break;
                    }
                }
                expectToken(TokenType.CloseCurly, "at the end of compound statement");
                return new CompoundStatement(open.location, statements);
            }
            default:
            if (isTypeKeyword(next.type)) {
                Token typeToken = consumeToken();
                Token name = expectToken(TokenType.Variable, "in definition statement");
                expectToken(TokenType.Equal);
                Expression definingExpression = parseExpression();
                expectToken(TokenType.SemiColon);
                return new DefinitionStatement(typeToken, name, definingExpression);
            } else {
                Expression expression = parseExpression();
                expectToken(TokenType.SemiColon, "at the end of statement");
                return new ExpressionStatement(expression);
            }

        }
    }
}
