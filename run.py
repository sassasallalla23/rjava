import subprocess as sub
import sys

if __name__ == '__main__':
    if len(sys.argv) < 3:
        print("Not enough arguments\nUsage: %s [source_file] [output_class_name]" % sys.argv[0])
        exit(0)
    p = sub.run(["java", "Main", sys.argv[1], sys.argv[2]])
    if p.returncode == 0:
        sub.run(["java", "-jar", "jasmin.jar", "%s.j" % sys.argv[2]])
    
