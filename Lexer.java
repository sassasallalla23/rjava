
/**
 * Write a description of class Lexer here.
 *
 * @author Johannes Bockhorst
 * @version 0.1
 */

import java.nio.file.Paths;
import java.nio.file.Files;
import java.nio.charset.StandardCharsets;
import java.util.Hashtable;
import java.util.ArrayList;

class Errors {
    public static void reportAtLocation(String message, Location location) {
        report(String.format("%s: %s\n", location.toString(), message));
    }

    public static void report(String message) {
        System.err.print(message);
        System.exit(1);
    }
}

class Location {
    public String filePath;
    public int row, col;

    public Location(String filePath, int row, int col) {
        this.filePath = filePath;
        this.row = row;
        this.col = col;
    }

    public String toString() {
        return String.format("%s:%d:%d", filePath, row+1, col+1);
    }

    public Location clone() {
        return new Location(filePath, row, col);
    }
}

enum TokenType {
    IntKeyword,
    WhileKeyword,
    IfKeyword,
    ElseKeyword,
    BooleanKeyword,
    TrueKeyword,
    FalseKeyword,
    CharKeyword,
    BreakKeyword,
    ContinueKeyword,

    Equal,
    Less,
    Greater,
    LessEqual,
    GreaterEqual,
    BangEqual,
    EqualEqual,
    Bang,
    Plus,
    Minus,
    Star,
    Slash,
    Percent,
    And,
    Pipe,
    LessLess,
    GreaterGreater,
    Tilde,
    PipePipe,
    AndAnd,
    Caret,
    Variable,

    IntLiteral,
    CharLiteral,

    SemiColon,
    Comma,
    OpenParenthesis,
    CloseParenthesis,
    CloseCurly,
    OpenCurly,
    EOF
}

class Token {
    public Location location;
    public TokenType type;
    public String text;

    public Token(Location location, TokenType type, String text) {
        this.location = location;
        this.type = type;
        this.text = text;
    }

    public String toString() {
        return String.format("[%s: \"%s\" (%s)]", location.toString(), text, type.toString());
    }
}

public class Lexer {
    public Location currentLocation;
    public String source;
    public int position;
    public Hashtable<String, TokenType> keywordTable;

    public Lexer(String filePath) {
        keywordTable = new Hashtable<>();
        keywordTable.put("int", TokenType.IntKeyword);
        keywordTable.put("boolean", TokenType.BooleanKeyword);
        keywordTable.put("char", TokenType.CharKeyword);
        keywordTable.put("true", TokenType.TrueKeyword);
        keywordTable.put("false", TokenType.FalseKeyword);
        keywordTable.put("if", TokenType.IfKeyword);
        keywordTable.put("while", TokenType.WhileKeyword);
        keywordTable.put("else", TokenType.ElseKeyword);
        keywordTable.put("break", TokenType.BreakKeyword);
        keywordTable.put("continue", TokenType.ContinueKeyword);

        try {
            source = Files.readString(Paths.get(filePath), StandardCharsets.US_ASCII);
        } catch (Exception e) {
            System.err.printf("ERROR: %s\n", e.toString());
            System.exit(1);
            source = null;
        }

        position = 0;
        currentLocation = new Location(filePath, 0, 0);
    }

    public char peekChar() {
        if (position >= source.length()) {
            return '\0';
        }

        return source.charAt(position);
    }

    public void advance() {
        if (position >= source.length()) return;
        char curr = peekChar();
        if (curr == '\n') {
            currentLocation.row++;
            currentLocation.col = 0;
        } else {
            currentLocation.col++;
        }
        position++;
        //System.out.println(source.length() - position);
    }

    public Token nextToken() {
        while (Character.isWhitespace(peekChar())) {
            advance();
        }
        Location start = currentLocation.clone();
        char next = peekChar();
        advance();
        switch (next) {
            case '=':
            if (peekChar() == '=') {
                advance();
                return new Token(start, TokenType.EqualEqual, "==");
            } else {
                return new Token(start, TokenType.Equal, "=");
            }
            case '<':
            if (peekChar() == '=') {
                advance();
                return new Token(start, TokenType.LessEqual, "<=");
            } else if (peekChar() == '<') {
                advance();
                return new Token(start, TokenType.LessLess, "<<");
            } else {
                return new Token(start, TokenType.Less, "<");
            }
            case '>':
            if (peekChar() == '=') {
                advance();
                return new Token(start, TokenType.GreaterEqual, ">=");
            } else if (peekChar() == '>') {
                advance();
                return new Token(start, TokenType.GreaterGreater, ">>");
            } else {
                return new Token(start, TokenType.Greater, ">");
            }
            case '!':
            if (peekChar() == '=') {
                advance();
                return new Token(start, TokenType.BangEqual, "!=");
            } else {
                return new Token(start, TokenType.Bang, "!");
            }
            case '+': return new Token(start, TokenType.Plus,    "+");
            case '-': return new Token(start, TokenType.Minus,   "-");
            case '*': return new Token(start, TokenType.Star,    "*");
            case '/':
            if (peekChar() == '/') {
                while (peekChar() != '\n' && peekChar() != '\0') advance();
                return nextToken();
            } else {
                return new Token(start, TokenType.Slash,   "/");
            }
            case '%': return new Token(start, TokenType.Percent, "%");
            case ',': return new Token(start, TokenType.Comma, ",");
            case '|': {
                char second = peekChar();
                if (second == '|') {
                    advance();
                    return new Token(start, TokenType.PipePipe, "||");
                } else {
                    //Errors.reportAtLocation(String.format("LEX_ERROR: Unexpected character '%c', expected '|'", second), start);
                    //System.exit(1);
                    return new Token(start, TokenType.Pipe, "|");
                }
            }
            case '&': {
                char second = peekChar();
                if (second == '&') {
                    advance();
                    return new Token(start, TokenType.AndAnd, "&&");
                } else {
                    //Errors.reportAtLocation(String.format("LEX_ERROR: Unexpected character '%c', expected '&'", second), start);
                    //System.exit(1);
                    return new Token(start, TokenType.And, "&");
                }
            }
            case '~': return new Token(start, TokenType.Tilde, "~");
            case '^': return new Token(start, TokenType.Caret, "^");
            case ';': return new Token(start, TokenType.SemiColon, ";");
            case '(': return new Token(start, TokenType.OpenParenthesis, "(");
            case ')': return new Token(start, TokenType.CloseParenthesis, ")");
            case '{': return new Token(start, TokenType.OpenCurly, "{");
            case '}': return new Token(start, TokenType.CloseCurly, "}");
            case '\0': return new Token(start, TokenType.EOF, "");
            case '\'': { // TODO: Add escape characters
                char character = peekChar();
                advance();
                if (peekChar() != '\'') {
                    Errors.reportAtLocation("LEX_ERROR: Unclosed character literal", currentLocation);
                }
                advance();
                return new Token(start, TokenType.CharLiteral, String.format("'%c'", character));
            }
            default: {
                if (Character.isDigit(next)) {
                    int startPos = position-1;
                    while (Character.isDigit(peekChar())) {
                        advance();
                    }
                    String number = source.substring(startPos, position);
                    return new Token(start, TokenType.IntLiteral, number);
                } else if (Character.isAlphabetic(next)) {
                    int startPos = position-1;
                    while (Character.isLetterOrDigit(peekChar())) {
                        advance();
                    }

                    String name = source.substring(startPos, position);
                    TokenType type = keywordTable.getOrDefault(name, TokenType.Variable);
                    return new Token(start, type, name);
                } else {
                    Errors.reportAtLocation("LEX_ERROR: Bad character input '" + next + "'", start);
                }
            } break;
        }
        return null;
    }

    public ArrayList<Token> collectTokens() {
        ArrayList<Token> tokens = new ArrayList<>();
        Token next;
        do {
            next = nextToken();  
            tokens.add(next);
        } while (next.type != TokenType.EOF);
        return tokens;
    }
}
