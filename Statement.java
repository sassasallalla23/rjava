
/**
 * Write a description of class Statement here.
 *
 * @author (your name)
 * @version (a version number or a date)
 */
import java.util.ArrayList;

abstract class Statement {
    public Location location;

    public Statement(Location location) {
        this.location = location;
    }

    public abstract void print(int indentation);

    public void print() {
        print(0);
    }

    public abstract void typeCheck(TypeChecker typeChecker);

    public abstract void emit(CodeGenerator codeGenerator);
}

class ExpressionStatement extends Statement {
    public Expression expression;

    public ExpressionStatement(Expression expression) {
        super(expression.location);
        this.expression = expression;
    }

    @Override
    public void print(int indentation) {
        Printing.printIndentation(indentation);
        System.out.println("Expression");
        expression.print(indentation + 1);
    }

    @Override
    public void typeCheck(TypeChecker typeChecker) {
        expression.resolveType(typeChecker);
    }

    @Override
    public void emit(CodeGenerator codeGenerator) {
        expression.emit(codeGenerator);
        if (!expression.type.equals(Types.VOID)) {
            codeGenerator.appendInstruction("pop", -1);
        }
    }
}

class DefinitionStatement extends Statement {
    public Token type;
    public Token name;
    public Expression definition;
    public int id;

    public DefinitionStatement(Token type, Token name, Expression definition) {
        super(type.location);
        this.type = type;
        this.name = name;
        this.definition = definition;
    }

    @Override
    public void print(int indentation) {
        Printing.printIndentation(indentation);
        System.out.printf("Definition(%s: %s)\n", name.text, type.text);
        definition.print(indentation+1);
    }

    @Override
    public void typeCheck(TypeChecker typeChecker) {
        Type variableType = Types.fromToken(type);
        if (variableType == null) {
            Errors.reportAtLocation(String.format("TYPE_ERROR: Unknown type <%s>", type.text), type.location);
        }
        Type foundType = definition.resolveType(typeChecker);
        if (!variableType.equals(foundType)) {
            Errors.reportAtLocation(String.format("TYPE_ERROR: Right side of definition <%s> does not match declared type <%s>", foundType.toString(), variableType.toString()), type.location);
        }
        if (typeChecker.getSymbol(name.text) != null) {
            Errors.reportAtLocation(String.format("TYPE_ERROR: Symbol \"%s\" already exists", name.text), name.location);
        }
        id = typeChecker.getVariableCount();
        typeChecker.addVariable(name.text, variableType);
    }

    @Override
    public void emit(CodeGenerator codeGenerator) {
        if (definition.type.equals(Types.INT) || definition.type.equals(Types.BOOLEAN) || definition.type.equals(Types.CHAR)) {
            definition.emit(codeGenerator);
            codeGenerator.appendInstruction(String.format("istore%c%d", id <= 3 ? '_' : ' ', id), -1);
        } else {
            Errors.report(String.format("Unimplemented type for variable %s", definition.type.toString()));
        }
    }
}


class WhileStatement extends Statement {
    public Expression condition;
    public Statement body;
    public ArrayList<Statement> controlFlowStatements;

    public WhileStatement(Expression condition, Statement body) {
        super(condition.location); // TODO: precision
        this.condition = condition;
        this.body = body;
        controlFlowStatements = new ArrayList<>();
    }

    @Override
    public void print(int indentation) {
        Printing.printIndentation(indentation);
        System.out.printf("While\n");
        Printing.printIndentation(indentation+1);
        System.out.printf("Condition:\n");
        condition.print(indentation+2);
        Printing.printIndentation(indentation+1);
        System.out.printf("Body:\n");
        body.print(indentation+2);        
    }

    @Override
    public void typeCheck(TypeChecker typeChecker) {
        condition.resolveType(typeChecker);
        if (!condition.type.equals(Types.BOOLEAN)) {
            Errors.reportAtLocation(String.format("TYPE_ERROR: Condition of while statement was expected to have type <%s>, instead got <%s>", Types.BOOLEAN.toString(), condition.type.toString()), condition.location);
        }
        typeChecker.whileStatementStack.push(this);
        body.typeCheck(typeChecker);
    }

    @Override
    public void emit(CodeGenerator codeGenerator) {
        String conditionLabel = codeGenerator.generateLabel();
        String endLabel = codeGenerator.generateLabel();
        for (Statement statement : controlFlowStatements) {
            if (statement instanceof BreakStatement) {
                ((BreakStatement)(statement)).label = endLabel;
            } else if (statement instanceof ContinueStatement) {
                ((ContinueStatement)(statement)).label = conditionLabel;
            } else {
                Errors.reportAtLocation("GENERERATION_ERROR: Unknown control flow statement", statement.location);
            }
        }
        codeGenerator.appendInstruction(String.format("%s:", conditionLabel), 0);
        condition.emit(codeGenerator);
        codeGenerator.appendInstruction(String.format("ifeq %s", endLabel), -1);
        body.emit(codeGenerator);
        codeGenerator.appendInstruction(String.format("goto %s", conditionLabel), 0);
        codeGenerator.appendInstruction(String.format("%s:", endLabel), 0);
    }
}

class BreakStatement extends Statement {
    public String label;
    public BreakStatement(Location location) {
        super(location);
        label = null;
    }
    
    @Override
    public void print(int indentation) {
        Printing.printIndentation(indentation);
        System.out.println("Break");
    }
    
    @Override
    public void typeCheck(TypeChecker typeChecker) {
        typeChecker.whileStatementStack.peek().controlFlowStatements.add(this);
    }
    
    @Override
    public void emit(CodeGenerator codeGenerator) {
        codeGenerator.appendInstruction(String.format("goto %s", label), 0);
    }
}

class ContinueStatement extends Statement {
    public String label;
    public ContinueStatement(Location location) {
        super(location);
        label = null;
    }
    
    @Override
    public void print(int indentation) {
        Printing.printIndentation(indentation);
        System.out.println("Continue");
    }
    
    @Override
    public void typeCheck(TypeChecker typeChecker) {
        if (typeChecker.whileStatementStack.isEmpty()) {
            Errors.reportAtLocation("TYPE_ERROR: Continue statement found outside of while loop", location);
        }
        typeChecker.whileStatementStack.peek().controlFlowStatements.add(this);
    }
    
    @Override
    public void emit(CodeGenerator codeGenerator) {
        codeGenerator.appendInstruction(String.format("goto %s", label), 0);
    }
}

class IfStatement extends Statement {
    Expression condition;
    Statement body;
    Statement elseBody;

    public IfStatement(Expression condition, Statement body, Statement elseBody) {
        super(condition.location); // TODO: precision
        this.condition = condition;
        this.body = body;
        this.elseBody = elseBody;
    }

    @Override
    public void print(int indentation) {
        Printing.printIndentation(indentation);
        System.out.printf("If\n");
        Printing.printIndentation(indentation+1);
        System.out.printf("Condition:\n");
        condition.print(indentation+2);
        Printing.printIndentation(indentation+1);
        System.out.printf("Body:\n");
        body.print(indentation+2); 
        if (elseBody != null) {
            Printing.printIndentation(indentation+1);
            elseBody.print(indentation+2);
        }
    }

    @Override
    public void typeCheck(TypeChecker typeChecker) {
        condition.resolveType(typeChecker);
        if (!condition.type.equals(Types.BOOLEAN)) {
            Errors.reportAtLocation(String.format("TYPE_ERROR: Condition of if statement was expected to have type <%s>, instead got <%s>", Types.BOOLEAN.toString(), condition.type.toString()), condition.location);
        }
        body.typeCheck(typeChecker);
        if (elseBody != null) {
            elseBody.typeCheck(typeChecker);
        }
    }

    @Override
    public void emit(CodeGenerator codeGenerator) {
        condition.emit(codeGenerator);
        if (elseBody == null) {
            String endLabel = codeGenerator.generateLabel();
            codeGenerator.appendInstruction(String.format("ifeq %s", endLabel), -1);
            body.emit(codeGenerator);
            codeGenerator.appendInstruction(String.format("%s:", endLabel), 0);
        } else {
            String endLabel = codeGenerator.generateLabel();
            String elseLabel = codeGenerator.generateLabel();
            codeGenerator.appendInstruction(String.format("ifeq %s", elseLabel), -1);
            body.emit(codeGenerator);
            codeGenerator.appendInstruction(String.format("goto %s", endLabel), 0);
            codeGenerator.appendInstruction(String.format("%s:", elseLabel), 0);
            elseBody.emit(codeGenerator);
            codeGenerator.appendInstruction(String.format("%s:", endLabel), 0);
        }
    }
}

class CompoundStatement extends Statement {
    ArrayList<Statement> subStatements;

    public CompoundStatement(Location start, ArrayList<Statement> subStatements) {
        super(start);
        this.subStatements = subStatements;
    }

    @Override
    public void print(int indentation) {
        Printing.printIndentation(indentation);
        System.out.println("Compound");
        for (Statement statement : subStatements) {
            statement.print(indentation+1);
        }
    }

    @Override
    public void typeCheck(TypeChecker typeChecker) {
        typeChecker.pushScope();
        int startVariableCount = typeChecker.getVariableCount();
        for (Statement statement : subStatements) {
            statement.typeCheck(typeChecker);
        }
        typeChecker.popScope();
        typeChecker.setVariableCount(startVariableCount);
    }

    @Override
    public void emit(CodeGenerator codeGenerator) {
        for (Statement statement : subStatements) {
            statement.emit(codeGenerator);
        }
    }
}
